# node-bbs

#### Description
a bbs build by node!

#### Software Architecture
Software architecture description:
整个项目使用前后端分离的结构, 初步创建6个项目, 前后端各三个.
##### 前端:
1.  cli.logic
包含前端与后端交互的通用代码逻辑.
2.  cli.web
Web端渲染程序, 使用React.
3.  cli.mp
小程序端的界面渲染, 使用Taro.
注: H5的界面做进Web端的适配活着Taro的多端编译.
##### 后端:
1.  ser.dal
与数据库进行交互.
2.  ser.Software
面向服务的但项目架构, 使用thinkjs的多模块模式, 方便拆分成微服务架构.
3.  ser.msa
微服务架构, 未来的扩展项目.
注: 未来后端后继续扩展核心逻辑项目等.

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
