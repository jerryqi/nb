import * as mysql from 'mysql';

const dbConf = {
  host: '192.168.64.2',
  user: 'root',
  password: '123456',
  database: 'nodebbs'
};

export class DB {
  static getConn(conf: mysql.ConnectionConfig = null): mysql.Connection {
    return mysql.createConnection(conf ? conf : dbConf);
  }

  static execute(query: (conn: mysql.Connection) => void) {
    let conn: mysql.Connection = this.getConn();
    conn.connect();
    query(conn);
  }
}